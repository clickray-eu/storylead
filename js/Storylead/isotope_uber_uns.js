// file: Modules/single-themen.item.js
$(document).ready(function () {
    $('.container.filtration-wrapper').cutWrappers('.single-themen-item');
    // first run isotope
    setTimeout(function () {
        $('.filtration-wrapper').isotope();
    }, 10);

    skillsInit2();
});

// end file: Modules/single-themen.item.js
// $(document).ready(function () {
//     //Skills animation
//     if ($(".loading-skills-wrapper").length > 0) {
//         skillsInit();
//     }
// });

function skillsInit() {
    var checkBars = 0;
    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        if ($(".loading-skills-wrapper").length > 0) {
            loadingBars();
        }
    });
    $(window).scroll();

    function loadingBars() {
        var scrollTop = $(window).scrollTop();
        var positionBars = $(".loading-circle").offset().top;
        var windowHeight = $(window).height()
        var scrollBottom = scrollTop + windowHeight;
        if (scrollBottom - 150 > positionBars && checkBars == 0) {
            checkBars = 1;

            $("section.loading-circle .circle").circliful({
                animation: 1,
                animationStep: 5,
                foregroundBorderWidth: 3,
                backgroundBorderWidth: 3,
                foregroundColor: '#FFF',
                backgroundColor: '#636362',
                fontColor: '#FFF',
                percentageTextSize: 30,
                multiPercentage: 1
            });
            $("section.loading-circle .circle .timer").attr('y', '110');
        }
    };
}

function skillsInit2() {
        
        
        $('.single-themen-item').hover(function() {
            
            if (!$(this).hasClass('initiated')) {
                $(this).addClass('initiated');
                loadingBars2($(this)); 
            }
        });

        function loadingBars2(elem){
                var percentBar1 = $(elem).find(".bar1 span").html();
                var percentBar2 = $(elem).find(".bar2 span").html();
                var percentBar3 = $(elem).find(".bar3 span").html();
                $(elem).find(".bar span").html(0+"%");
                
                $(elem).find(".wrapper-bars .loading-bar").css('width', 0+"%");
               
                setTimeout( function(){ 
                    $(elem).find(".wrapper-bars .bar1 .loading-bar").css('width', percentBar1+"%");
                    $(elem).find(".bar1 span").html(percentBar1+"%");
                }, 100);
                setTimeout( function(){ 
                    $(elem).find(".wrapper-bars .bar2 .loading-bar").css('width', percentBar2+"%");
                    $(elem).find(".bar2 span").html(percentBar2+"%");
                }, 500);
                setTimeout( function(){ 
                    $(elem).find(".wrapper-bars .bar3 .loading-bar").css('width', percentBar3+"%");
                    $(elem).find(".bar3 span").html(percentBar3+"%");
                }, 900);

                
            
    }
}

