var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
if (iOS) {
    $('body').addClass('ios');
}

if (isSafari) {
    $('body').addClass('safari');
}
if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) ||/Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1 ){
    $('body').addClass('ms-browser');
}