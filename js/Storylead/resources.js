$(document).ready(function () {
    $('.resource-categories__list.second-list').closest('li').width($('.resource-categories__list.second-list').outerWidth() - 40);
    $('.resource-categories__list.first-list').closest('li').width($('.resource-categories__list.first-list').outerWidth() - 40);
    $('.resource-categories__list.first-list li').on('click', function () {
        var clickText = $(this).text();
        $('#first-list-expand-btn').text(clickText);
    });
    $('.resource-categories__list.second-list li').on('click', function () {
        var clickText = $(this).text();
        $('#second-list-expand-btn').text(clickText);
    });

    toggleMenus('#first-list-expand-btn', '.resource-categories__list.first-list');
    toggleMenus('#second-list-expand-btn', '.resource-categories__list.second-list');
    showResourcePopup();
})
function toggleMenus(btn, listToToggle) {
    $(btn).on('click', () => {
        $(listToToggle).fadeToggle();
    });
    $(listToToggle + ' li').on('click', () => {
        $(listToToggle + ' li').parent().fadeOut();
    })
}
function showResourcePopup(){
    $('.resource.videoModule .resource-play-video-top, .resource.videoModule .resource-play-video').on('click', function(e){
        e.preventDefault();
        $(this).closest('.resource').find('.resource__video-popup').fadeIn();
        // $(this).closest('.resource').find('video, iframe')[0].play();
        $('body').css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        })
    })
    $('.resource.videoModule .exit').on('click', function(){
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        })
    })
    $('.resource.videoModule .video-wrapper').on('click', function(e){
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        })
    })
    $('.resource.videoModule .video-wrapper iframe, .resource.videoModule .video-wrapper video').on('click', function(e){
        e.stopPropagation();
    })
}


function showResource(openBtn, exitBtn, popupWrapper, popupInner) {
    let button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', (e) => {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', () => {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        })
    });

    form.on('click', (e) => {
        e.stopPropagation();
    });

    if (afterLoad.length>0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}


