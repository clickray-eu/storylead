$(document).ready(function(){
    referenzeSlider();
});




function referenzeSlider(){
    $('.referenzen-slider-wrapper > span').slick({
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 960,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 720,
              settings: {
                slidesToShow: 2
              }
            },
            {
              breakpoint: 490,
              settings: {
                slidesToShow: 1
              }
            }
          ]

    });
}