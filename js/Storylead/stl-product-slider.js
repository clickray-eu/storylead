$(document).ready(function() {
	$('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
		infinite: true,
		autoplay: true,
		dots: false,
		arrows: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
		      breakpoint: 991,
		      settings: {
		      	arrows: false,
		        slidesToShow: 2,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 767,
		      settings: {
		      	arrows: false,
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		]
	});
})