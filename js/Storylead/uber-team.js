$("a.play").click(function() {

    var videoSrc = $(this).data('video');
    var $this = $(this);
    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>',
        },
        callbacks: {
            open: function() {

                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();

            },
            close: function() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();

            }
        }
    });

});




    setTimeout(function(){ $('.filters li a.active').click(); }, 10);
    $(".questions").append($(".questions .question"));
    $(".questions >.span12").remove();
    $(".filters").append($(".filters .filter"));
    $(".filters >.span12").remove();
    var $container = $('.questions');
    
    // filter items when filter link is clicked
    $('.filters li a').on('click', function() {
        $(".faq .questions .question").css('position', 'absolute');
        $(".faq .questions .question h3").removeClass('rotate');
        $(".faq .questions .question div.text").hide(true);
        $('.filters li a').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).data('filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });


    if ($(".faq").length > 0) {
        $(".faq .questions .question h3").click(function(i) {
            if ($(this).hasClass("rotate")) {
                $(this).parent().children("div.text").slideUp("fast");
                $(this).removeClass("rotate")
            } else {
                $(".faq .questions .question div.text").slideUp("fast");
                $(".faq .questions .question h3").removeClass("rotate");
                $(this).addClass("rotate");
                $(this).parent().children("div.text").slideDown("fast");
                $(".faq .questions .question").css("position", "static")
            }
        })
    }
