$(document).ready(function(){
    $('.header--main .hs-menu-children-wrapper').each(function(){

        $(this).find('li').wrapAll("<div class='container'></div>");

    })

    headerOnScroll();
    activeMobileMenu();
    slickMenuInt();
    checkMenuLength();

})

function checkMenuLength(){
    $('.hs-menu-item.hs-menu-depth-1.hs-item-has-children').each(function(){
        if ($(this).find('.hs-menu-children-wrapper .container li').length < 4){
            $(this).find('.hs-menu-children-wrapper .container').addClass('centered');
        }
    })
}
// White background on scroll (landing page)
function headerOnScroll() {
    const header = $('.stl-lp-2017 .header--main');
    let logo = $('.logo').find('img');
    let oldSrc = logo.attr('src');
    let newSrc = oldSrc.replace("-white", "");

   // oldSrc = 'https://www.storylead.com/hs-fs/hubfs/ADMIN/STL_CORPORATE_DESIGN_ELEMENTS/Logo/storylead-new-logo-white.png';
  //  newSrc = 'https://www.storylead.com/hs-fs/hubfs/ADMIN/STL_CORPORATE_DESIGN_ELEMENTS/Logo/storylead-new-logo.png';

    

    $(window).on("scroll", () => {
        const scrollPosition = $(window).scrollTop();

        if ( scrollPosition > 0 ) {
            header.addClass('header--white');
            logo.attr('src', newSrc);
            logo.attr('srcset', newSrc);
        } else {
            header.removeClass('header--white');
            logo.attr('src', oldSrc);
            logo.attr('srcset', oldSrc);
        }
        
    })
}


// Resize

$(window).resize(function(){
 activeMobileMenu();
})

// FUNCTIONS

function activeMobileMenu() {

    var deviceWidth = $(window).innerWidth();

   
    if(deviceWidth < (1140 - 16)){
        $('header').addClass('mobile-menu');
        $('.body-container-wrapper').addClass('mobile-menu');
    }else{
        $('header').removeClass('mobile-menu');
        $('.body-container-wrapper').removeClass('mobile-menu');
    }

}

function slickMenuInt() {

    $('.main-menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.header--main > .span12 > div:last-child > div',
        allowParentLinks: true,
        init:function(){
            var old_google_search=$(".slicknav_nav li .hs_cos_wrapper_type_google_search");
            var desktop_google_search=$(".main-menu .hs_cos_wrapper_type_google_search").clone(true);
            old_google_search.after(desktop_google_search);
            old_google_search.remove();
            $(".slicknav_nav li .hs_cos_wrapper_type_google_search input[type='text']").keyup(function(){
                $(".main-menu .hs_cos_wrapper_type_google_search input[type='text']").attr("value",$(this).val());
            });
        }
    });
} 
