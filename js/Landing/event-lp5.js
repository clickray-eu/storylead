$(document).ready(function () {

    initBrandsSlider();

});


function initBrandsSlider(){
        if($(".brands-slider").length>0){
            $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow:5,
            slidesToScroll: 5,
            arrows: true,
            dots: true,
              responsive: [
                {
                  breakpoint: 1150,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                  }
                }
,
                {
                  breakpoint: 900,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                  }
                }
,
                {
                  breakpoint: 760,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
            ]
        });
}
}