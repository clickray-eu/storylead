$(window).load(function() {
	if ($('.stl-survey').hasClass('stl-survey-questions')) {
		$('.stl-survey-hero-banner__icon-box').first().addClass('stl-survey-hero-banner__icon-box--active');
	} else if ($('.stl-survey').hasClass('stl-survey-answers') == true) {
		highlightAnswerOnion();
	} else if ($('.stl-survey').hasClass('stl-survey-typ') == true) {
		$('.stl-survey-hero-banner__icon-box').addClass('stl-survey-hero-banner__icon-box--active');
	}
	addRangeInputs();
	makeSections();
	changeSection();
	changeHiddenInputVal();
});

function highlightAnswerOnion() {
	$('.stl-survey-answer').each(function() {
		var sectionNum = parseInt($(this).data('section')) + 1;
		$('.stl-survey-hero-banner__icon-box:nth-of-type(' + sectionNum + ')').addClass('stl-survey-hero-banner__icon-box--active');
	});
}

function addRangeInputs() {
	$('[class^="hs_business_check_negative"], [class^="hs_business_check_positive"]').each(function() {
		$(this).append('<div class="stl-survey-range-slider-wrapper">'
						 + '<div class="stl-survey-range-slider-labels"><p>UNZUFRIEDEN</p><p>ZUFRIEDEN</p></div>'
						 + '<input class="stl-survey-range-slider stl-survey-range-slider--unchanged" type="range" min="0" max="10" value="0">'
						 + '<div class="stl-survey-range-slider-legend"><span>0</span><span>1</span><span>2</span><span>3</span><span>4</span><span>5</span><span>6</span><span>7</span><span>8</span><span>9</span><span>10</span></div>'
					 + '</div>');
	});
}

function makeSections() {
	for (var i=1; i<=5; i++) {
		$('.stl-survey-body form').append('<div class="stl-survey-section stl-survey-section-' + i + '"></div>')
	}
	
	$('.stl-survey-section').first().addClass('stl-survey-section--active');

	var sectionStory = $('[name^="business_check_"][name$="_story"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
	var sectionLeads = $('[name^="business_check_"][name$="_leads"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
	var sectionDeals = $('[name^="business_check_"][name$="_deals"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
	var sectionGrowth = $('[name^="business_check_"][name$="_growth"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();

	$('.stl-survey-section-1').append(sectionStory);
	$('.stl-survey-section-2').append(sectionLeads);
	$('.stl-survey-section-3').append(sectionDeals);
	$('.stl-survey-section-4').append(sectionGrowth);

	//append all other fields to last section
	$('.stl-survey-body form > fieldset').each(function() {
		$('.stl-survey-section-5').append($(this));
	})

	$('.stl-survey-questions .widget-type-form form').css('display', 'block');
}

function validateForm() {
	if ($('.stl-survey-section--active [name^="business_check_positive"]').val() != '' && $('.stl-survey-section--active [name^="business_check_negative"]').val() != '') {
		return true;
	} else {
		return false;
	}
}

function changeHiddenInputVal() {
	$('input.stl-survey-range-slider').on('input change, click', function() {
		$(this).removeClass('stl-survey-range-slider--unchanged');
		var rangeVal = $(this).val();
		var hiddenInput = $(this).parent().siblings('.input').find('input.hs-input');
		hiddenInput.attr('value', rangeVal);

		if (validateForm() == true) {
			$('.stl-survey-nav .btn-next').removeClass('btn-disabled');
		}

		if ($(window).width() < 767) {
			var legend = $(this).siblings('.stl-survey-range-slider-legend');
			legend.find('span').css('opacity', '0')
			legend.find('span:nth-child(' + (parseInt(rangeVal)+1) + ')').css('opacity', '1')
		} else {
			$('.stl-survey-range-slider-legend span').removeAttr('style');
		}
	});

}

function calculateVop() {
    var negativeStory = parseInt($('input[name$=_negative_story]').val());
    var positiveStory = parseInt($('input[name$=_positive_story]').val());
    var vopStory = $('input[name$=_vop_story]');
    var vopStoryResult = positiveStory - negativeStory;

    var negativeLeads = parseInt($('input[name$=_negative_leads]').val());
    var positiveLeads = parseInt($('input[name$=_positive_leads]').val());
    var vopLeads = $('input[name$=_vop_leads]');
    var vopLeadsResult = positiveLeads - negativeLeads;

    var negativeDeals = parseInt($('input[name$=_negative_deals]').val());
    var positiveDeals = parseInt($('input[name$=_positive_deals]').val());
    var vopDeals = $('input[name$=_vop_deals]');
    var vopDealsResult = positiveDeals - negativeDeals;

    var negativeGrowth = parseInt($('input[name$=_negative_growth]').val());
    var positiveGrowth = parseInt($('input[name$=_positive_growth]').val());
    var vopGrowth = $('input[name$=_vop_growth]');
    var vopGrowthResult = positiveGrowth - negativeGrowth;

    var highestVopInput = $('input[name=business_check_highest_vop]');
	var highestVop = 0;
	var highestVopSection = "";
    
    var vopArray = {
        Story: vopStoryResult,
        Leads: vopLeadsResult,
        Deals: vopDealsResult,
        Growth: vopGrowthResult
    }

	$.each(vopArray, function(key, value) {
		if (value > highestVop) {
			highestVop = value;
			highestVopSection = key;
        }
	});

	highestVopInput.val(highestVopSection);

    vopStory.val(vopStoryResult);
    vopLeads.val(vopLeadsResult);
    vopDeals.val(vopDealsResult);
    vopGrowth.val(vopGrowthResult);
}

function changeSection() {
	$('.stl-survey-nav .btn-next').on('click', function() {

		// trigger click on form hidden submit button when in last section
		if ($('.stl-survey-section-5').hasClass('stl-survey-section--active')) {
			calculateVop();
			$('.stl-survey-body .hs_submit input[type="submit"]').click();
		}

		if (validateForm() == true && $('.stl-survey-section--active').next('.stl-survey-section').length > 0) {

			if ($('.stl-survey-section--active').hasClass('stl-survey-section--valid') == false)
			setTimeout(function() {
				$('html, body').animate({
			        scrollTop: $('.stl-survey-section--active').offset().top - 100
			    }, 500);
			}, 750);

			var nextSection = $('.stl-survey-section--active').next('.stl-survey-section');
			$('.stl-survey-section--active').addClass('stl-survey-section--valid')
			$('.stl-survey-section').removeClass('fadeOutRight');
			$('.stl-survey-section').removeClass('fadeInLeft');
			$(".stl-survey-section--active").addClass("wow fadeOutLeft animated").attr('data-wow-duration', '0.5s');
    		$(".stl-survey-section--active").attr("style","visibility: visible; animation-name: fadeOutLeft;");
			$('.stl-survey-section').removeClass('stl-survey-section--active');
			nextSection.addClass('stl-survey-section--active');
			$(".stl-survey-section--active").addClass("wow fadeInRight animated").attr('data-wow-duration', '0.5s');;
    		$(".stl-survey-section--active").attr("style","visibility: visible; animation-name: fadeInRight;");

			$('.stl-survey-nav .btn-prev').removeClass('btn-disabled');
			$('.stl-survey-nav .btn-next').addClass('btn-disabled');

			if ($('.stl-survey-section-5').hasClass('stl-survey-section--active') == false) {
				$('.stl-survey-hero-banner__icon-box--active').find('.inner-rect-mask').addClass('inner-rect-mask--active');
				var nextHeroIcon = $('.stl-survey-hero-banner__icon-box--active').next('.stl-survey-hero-banner__icon-box');
				
				if ($(window).width() > 767) {
					nextHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
				} else {
					$('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').removeClass('stl-survey-hero-banner__icon-box--active');
					nextHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
				}
			}

			// validate again for next section. If true, btn will be clickable
			if (validateForm() == true) {
				$('.stl-survey-nav .btn-next').removeClass('btn-disabled');
			}
		}
	})
	$('.stl-survey-nav .btn-prev').on('click', function() {
			if ($('.stl-survey-section-1.stl-survey-section--active').length == 0 && $('.stl-survey-section--active').prev('.stl-survey-section').length > 0) {
				var prevSection = $('.stl-survey-section--active').prev('.stl-survey-section');

				if ($('.stl-survey-section-5').hasClass('stl-survey-section--active') == false) {
					$('.inner-rect-mask.inner-rect-mask--active').last().removeClass('inner-rect-mask--active');

					if ($(window).width() > 767) {
						$('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').last().removeClass('stl-survey-hero-banner__icon-box--active');
					} else {
						var prevHeroIcon = $('.stl-survey-hero-banner__icon-box--active').last().prev('.stl-survey-hero-banner__icon-box');
						$('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').removeClass('stl-survey-hero-banner__icon-box--active');
						prevHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
					}

				}

				$('.stl-survey-section').removeClass('fadeOutLeft');
				$('.stl-survey-section').removeClass('fadeInRight');
				$(".stl-survey-section--active").addClass("wow fadeOutRight animated");
    			$(".stl-survey-section--active").attr("style","visibility: visible; animation-name: fadeOutRight;");
				$('.stl-survey-section').removeClass('stl-survey-section--active');
				prevSection.addClass('stl-survey-section--active');
				$(".stl-survey-section--active").addClass("wow fadeInLeft animated");
    			$(".stl-survey-section--active").attr("style","visibility: visible; animation-name: fadeInLeft;");

				$('.stl-survey-nav .btn-next').removeClass('btn-disabled');
				$('.stl-survey-nav .btn-prev').removeClass('btn-disabled');

				// always lock btn for first section
				if ($('.stl-survey-section-1.stl-survey-section--active').length > 0) {
					$('.stl-survey-nav .btn-prev').addClass('btn-disabled');
				}
			}	
		})
}