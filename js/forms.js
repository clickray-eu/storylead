// Functions for dropdown
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            // Zmienilem val() na text()
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>')
            }
        })
    })

    $form.find('.dropdown_select.input .dropdown-header').click(function (event, element) {
        if (!($(this).hasClass('slide-down'))) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });
    
    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    })
}
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", function () {
    $('form select').on('change', function () {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});