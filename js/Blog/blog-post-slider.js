$(document).ready(function() {
	$('.blog-post-slider').slick({
		dots: false,
		arrows: true,
		infinite: true,
		autoplay: true,
		speed: 300,
		slidesToShow: 1,
		adaptiveHeight: true
	});
});