$(document).ready(function() {
	productTableInit();
	changeProductTable();
	productAccordions();
});

function productTableInit() {
	var navItem = 0;
	var tableItem = 0;
	$('.product-table-left-nav__item').each(function() {
		$(this).attr('data-item', navItem);
		navItem++;
	});

	$('.product-table').each(function() {
		$(this).attr('data-item', tableItem);

		if ($(this).siblings('.product-related').length > 0) {
			$(this).siblings('.product-related').attr('data-item', tableItem);
			$('.product-add').append($(this).siblings('.product-related'));
		}

		tableItem++;
	});


}


function changeProductTable() {
  var activeTableId = $('.product-table-left-nav__item--active').data('item');
  var activeTableName = $('.product-table-left-nav__item--active').attr('id');

  $('.product-table[data-item=' + activeTableId + ']').addClass('product-table--active').fadeIn().show();
  $('.product-related[data-item=' + activeTableId + ']').addClass('product-related--active').fadeIn().show();

  $('.product-table-left-nav__item').on('click', function () {
    if ($(this).hasClass('product-table-left-nav__item--active')) {
      return false;
    } else {
      $('.product-table-left-nav__item').removeClass('product-table-left-nav__item--active');
      $(this).addClass('product-table-left-nav__item--active');
      activeTableId = $(this).data('item');
      $('.product-table').removeClass('product-table--active').fadeOut().hide();
      $('.product-related').removeClass('product-related--active').fadeOut().hide();;
      $('.product-table[data-item=' + activeTableId + ']').addClass('product-table--active').fadeIn().show();
      $('.product-related[data-item=' + activeTableId + ']').addClass('product-related--active').fadeIn().show();

      activeTableName = $('.product-table-left-nav__item--active').attr('id');
      var changeUrl = location.protocol + "//" + location.host + location.pathname + "?selected=" + activeTableName;
      window.history.replaceState({}, document.title, changeUrl);
    }

    if ($(window).width() < 768) {
      $('html, body').animate({
        scrollTop: $('.product-table[data-item=' + activeTableId + ']').first().offset().top - 100
      }, 500);
    }
  });

  if (GetURLParameter("selected") != undefined) {
    var selectedTable = GetURLParameter("selected");
    $('.product-table-left-nav__item#' + selectedTable).click();
    var changeUrl = location.protocol + "//" + location.host + location.pathname + "?selected=" + activeTableName;
    window.history.replaceState({}, document.title, changeUrl);
  }
}



function productAccordions() {
	$('.product-table__accordions a').on('click', function(e) {
		e.preventDefault();
	});

	$('.product-table__accordions .accordion-depth-1 > a').on('click', function(e) {
		if ($(this).parents('.accordion-depth-1').hasClass('expand')) {
			$('.product-table__accordions .accordion-depth-1').removeClass('expand');
			$('.product-table__accordions .accordion-depth-1 ul').slideUp();
		} else {
			$('.product-table__accordions .accordion-depth-1').removeClass('expand');
			$('.product-table__accordions .accordion-depth-1 ul').slideUp();
			$(this).parents('.accordion-depth-1').addClass('expand');
			$(this).parents('.accordion-depth-1').find('ul').slideDown();
		}
	})
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}