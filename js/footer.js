function showPopup(openBtn, exitBtn, popupWrapper, popupInner) {
    let button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', (e) => {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', () => {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        })
    });

    form.on('click', (e) => {
        e.stopPropagation();
    });

    if (afterLoad.length > 0 && $('.content-wrapper').find(afterLoad).length > 0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}

// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// 
// Suggested HTML:
// <a href="javascript:void(0);" id="footer-popup" class="cta--white">JETZT ANMELDEN</a>
// <div class="popup-wrapper">
//     <div class="exit"></div>
//     <div class="content-wrapper">
//         {# {% form "footer_form_popup", form_to_use='FormID', overrideable='false', response_message='<span class="h3" style="text-align: center; padding: 0 30px;">Thank you for contacting us. We will get in touch with you as soon as possible.</span>',response_response_type=inline %} #}
//     </div>
// </div>
//
// showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper')
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};
$(document).ready(function(){
    showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper')
    showPopup('#subscribe-popup', '.subscribe-popup-wrapper .exit', '.subscribe-popup-wrapper', '.subscribe-popup-wrapper .content-wrapper')
})
