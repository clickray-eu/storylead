'use strict';

// file: cutWrapper.js
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);

$(".pricing-box__container").cutWrappers('.pricing-box');
// end file: cutWrapper.js

// file: footer.js
function showPopup(openBtn, exitBtn, popupWrapper, popupInner) {
    var button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', function (e) {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        });
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if (afterLoad.length > 0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}

// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// 
// Suggested HTML:
// <a href="javascript:void(0);" id="footer-popup" class="cta--white">JETZT ANMELDEN</a>
// <div class="popup-wrapper">
//     <div class="exit"></div>
//     <div class="content-wrapper">
//         {# {% form "footer_form_popup", form_to_use='FormID', overrideable='false', response_message='<span class="h3" style="text-align: center; padding: 0 30px;">Thank you for contacting us. We will get in touch with you as soon as possible.</span>',response_response_type=inline %} #}
//     </div>
// </div>
//
// showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper')
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};
$(document).ready(function () {
    showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper');
    showPopup('#subscribe-popup', '.subscribe-popup-wrapper .exit', '.subscribe-popup-wrapper', '.subscribe-popup-wrapper .content-wrapper');
});

// end file: footer.js

// file: forms.js
// Functions for dropdown
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            // Zmienilem val() na text()
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>');
            }
        });
    });

    $form.find('.dropdown_select.input .dropdown-header').click(function (event, element) {
        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    });
}
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", function () {
    $('form select').on('change', function () {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});
// end file: forms.js

// file: header.js
$(document).ready(function () {
    $('.header--main .hs-menu-children-wrapper').each(function () {

        $(this).find('li').wrapAll("<div class='container'></div>");
    });

    headerOnScroll();
    activeMobileMenu();
    slickMenuInt();
});

// White background on scroll (landing page)
function headerOnScroll() {
    var header = $('.stl-lp-2017 .header--main');
    var logo = $('.logo').find('img');
    var oldSrc = logo.attr('src');
    var newSrc = oldSrc.replace("_white", "");

    $(window).on("scroll", function () {
        var scrollPosition = $(window).scrollTop();

        if (scrollPosition > 0) {
            header.addClass('header--white');
            logo.attr('src', newSrc);
        } else {
            header.removeClass('header--white');
            logo.attr('src', oldSrc);
        }
    });
}

// Resize

$(window).resize(function () {
    activeMobileMenu();
});

// FUNCTIONS

function activeMobileMenu() {

    var deviceWidth = $(window).innerWidth();

    if (deviceWidth < 1140 - 16) {
        $('header').addClass('mobile-menu');
        $('.body-container-wrapper').addClass('mobile-menu');
    } else {
        $('header').removeClass('mobile-menu');
        $('.body-container-wrapper').removeClass('mobile-menu');
    }
}

function slickMenuInt() {

    $('.main-menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.header--main > .span12 > div:last-child > div',
        allowParentLinks: true,
        init: function init() {
            var old_google_search = $(".slicknav_nav li .hs_cos_wrapper_type_google_search");
            var desktop_google_search = $(".main-menu .hs_cos_wrapper_type_google_search").clone(true);
            old_google_search.after(desktop_google_search);
            old_google_search.remove();
            $(".slicknav_nav li .hs_cos_wrapper_type_google_search input[type='text']").keyup(function () {
                $(".main-menu .hs_cos_wrapper_type_google_search input[type='text']").attr("value", $(this).val());
            });
        }
    });
}

// end file: header.js

// file: template.js
$(document).ready(function () {
    //Fixed Menu
    fixedMenu();
    //Mobile Menu
    mobileMenuInit();
    //Scroll down in header
    headerScrollDown();
    //Scroll up in footer
    footerScrollUp();
    //WOW animation init
    wowInit();

    //ONE, TWO, THREE COLUMN HeroSlider
    smallSliderInit();

    //Homepage HeroSlider
    homeSliderInit();

    //Homepage testimonials
    testimonialsSliderInit();

    //Homepage Services
    var brake_array = {};
    brake_array["1200"] = 4;
    brake_array["992"] = 3;
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    // rowModuleBreaker(".homepage .services",".box-services",brake_array,"first","last");

    //Homepage Features
    var brake_array = {};
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    // rowModuleBreaker(".homepage .features",".feature",brake_array,"first","last");

    //Skills animation
    if ($(".loader1").length > 0 && $(".loader2").length > 0 && $(".loader3").length > 0 && $(".loader4").length > 0) skillsInit();
    questionAccordion();

    //test

    scrollToAnchor();
});
function wowInit() {
    wow = new WOW({
        mobile: false
    });
    wow.init();
}
function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
function headerScrollDown() {
    $(".mouse_scroll").click(function () {
        $("html, body").animate({
            scrollTop: $("#main_section").position().top - 70
        }, 1000);
        return false;
    });
}
function footerScrollUp() {
    $("#arrow-up").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}
function testimonialsSliderInit() {
    $('.slick-testimonial > div > span').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}
function mobileMenuInit() {
    $("header.header .hs-menu-flow-horizontal > ul").slicknav({
        prependTo: "header.header",
        label: "",
        allowParentLinks: true
    });
    $(".slicknav_btn").click(function (i) {
        console.log("test");
        if ($("header.header.fixed-small").length > 0) {
            $("header.header").removeClass("fixed-small");
        } else {
            $("header.header").addClass("fixed-small");
        }
    });
}
function homeSliderInit() {
    $(".main .homeslider > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function smallSliderInit() {

    $(".sm-slider-main .slide-content > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function fixedMenu() {
    if ($(".header--static").length < 1 && $(".contact-template .header").length == 0) {
        $(window).scroll(function () {
            var i = $(window).scrollTop();
            if (i > 0) {
                $("header.header").addClass("fixed");
                if ($(".menu-black").length > 0) {
                    $(".menu-black .header .black").css("display", "none");
                    $(".menu-black .header .white").css("display", "block");
                }
            } else {
                if (i == 0) {
                    $("header.header").removeClass("fixed");
                    if ($(".menu-black").length > 0) {
                        $(".menu-black .header .black").css("display", "block");
                        $(".menu-black .header .white").css("display", "none");
                    }
                }
            }
        });
    }
    if ($(".contact-template .header").length > 0) {
        $(".contact-template .header").addClass("fixed");
    }
}
function rowModuleBreaker(parent, css_selector, break_array, class_first_row, class_last_row) {
    var oldCols = 0;
    $(window).resize(function () {
        var cols = 0;
        var lastBreak = 0;
        for (var windowBreak in break_array) {
            if (window.innerWidth <= parseInt(windowBreak)) {
                cols = break_array[windowBreak];
                break;
            }
            lastBreak = windowBreak;
        }

        if (cols == 0) {
            cols = break_array[lastBreak];
        }
        if (oldCols == 0 || oldCols != cols) {
            $(parent).each(function (j, selector) {

                var old = oldCols;
                oldCols = cols;
                var inc = -1;
                var row = [];
                var length = $(selector).find(css_selector).length;
                $(selector).find(css_selector).each(function (i, e) {
                    if (i % cols == 0) {
                        inc++;
                        row = [];
                    }
                    row.push($(e).parent().clone(true));
                    $(e).parent().parent().addClass("old_remove");
                    if (i % cols == cols - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        if (i == length - 1) {
                            rowContainer.addClass(class_last_row);
                        }
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }

                    if (i % cols < cols - 1 && i == length - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        rowContainer.addClass(class_last_row);
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }
                    if ($(e).parent().parent().hasClass("hs_cos_wrapper_type_custom_widget")) {
                        $(e).parent().parent().remove();
                    }
                });
                $(selector).find(css_selector).parent().attr("class", "span" + 12 / cols);
                $(".old_remove").remove();
            });
        }
    });
    $(window).resize();
}
function skillsInit() {
    var r = 0;
    var j = $(".loader1").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var h = $(".loader2").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var g = $(".loader3").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var f = $(".loader4").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    $(window).scroll(function () {
        var w = $(window).scrollTop();
        var t = $(".skills").position().top;
        var z = $(window).height();
        var u = w + z;
        var i = $(".skills .circle1").html();
        var y = $(".skills .circle2").html();
        var x = $(".skills .circle3").html();
        var v = $(".skills .circle4").html();
        if (u - 200 > t && r == 0) {
            r = 1;
            j.setPercent(i).draw();
            h.setPercent(y).draw();
            g.setPercent(x).draw();
            f.setPercent(v).draw();
        }
    });
}

function scrollToAnchor() {
    $('a[href*="#"]').on('click', function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name="' + this.hash.slice(1) + '"]');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 500);
                return false;
            }
        }
    });
}

// end file: template.js

// file: Landing/event-lp5.js
$(document).ready(function () {

    initBrandsSlider();
});

function initBrandsSlider() {
    if ($(".brands-slider").length > 0) {
        $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 5,
            slidesToScroll: 5,
            arrows: true,
            dots: true,
            responsive: [{
                breakpoint: 1150,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 900,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }
}
// end file: Landing/event-lp5.js

// file: Storylead/browser-check.js
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
if (iOS) {
    $('body').addClass('ios');
}

if (isSafari) {
    $('body').addClass('safari');
}
if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    $('body').addClass('ms-browser');
}
// end file: Storylead/browser-check.js

// file: Storylead/count-to.js
$(window).scroll(function () {

    checkCounterPosition();
});

$(document).ready(function () {

    checkCounterPosition();
});

// VARIABELS 


var checkcounter = 0;
var arrOfElements = getNumber('.counter-box-stl .count-to');
var act = 0;

// FUNCTIONS

function getNumber(element) {

    var arrNumber = [];

    $(element).each(function (i, e) {

        arrNumber[i] = $(e).data('count');
    });
    return arrNumber;
}

function countTo() {
    checkcounter = 1;
    $('.counter-box-stl .count-to').each(function (i, e) {

        if (act <= arrOfElements[i]) {
            $(e).text(act);
        }
    });

    act = act + 1;

    $(arrOfElements).each(function (i, e) {
        if (act <= e) {
            setTimeout(countTo, 100);
        }
    });
}

function checkCounterPosition() {
    var counterposition;
    if ($(".counter-box-stl .count-to").length != 0) {
        counterposition = $(".counter-box-stl .count-to").position().top + 100;
    }

    if ($(".counter-background .counter-box-stl .count-to").length != 0) {
        counterposition = $(".counter-background ").position().top + 200;
    }
    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
        countTo();
    }
}
// end file: Storylead/count-to.js

// file: Storylead/global.js

$('#arrow-up').on('click', function () {
    $('body, html').animate({
        scrollTop: 0
    }, 500);
});
// end file: Storylead/global.js

// file: Storylead/home.js
$(document).ready(function () {
    referenzeSlider();
});

function referenzeSlider() {
    $('.referenzen-slider-wrapper > span').slick({
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 960,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 720,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 490,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]

    });
}
// end file: Storylead/home.js

// file: Storylead/resources.js
$(document).ready(function () {
    $('.resource-categories__list.second-list').closest('li').width($('.resource-categories__list.second-list').outerWidth() - 40);
    $('.resource-categories__list.first-list').closest('li').width($('.resource-categories__list.first-list').outerWidth() - 40);
    $('.resource-categories__list.first-list li').on('click', function () {
        var clickText = $(this).text();
        $('#first-list-expand-btn').text(clickText);
    });
    $('.resource-categories__list.second-list li').on('click', function () {
        var clickText = $(this).text();
        $('#second-list-expand-btn').text(clickText);
    });

    toggleMenus('#first-list-expand-btn', '.resource-categories__list.first-list');
    toggleMenus('#second-list-expand-btn', '.resource-categories__list.second-list');
    showResourcePopup();
});
function toggleMenus(btn, listToToggle) {
    $(btn).on('click', function () {
        $(listToToggle).fadeToggle();
    });
    $(listToToggle + ' li').on('click', function () {
        $(listToToggle + ' li').parent().fadeOut();
    });
}
function showResourcePopup() {
    $('.resource.videoModule .resource-play-video-top, .resource.videoModule .resource-play-video').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.resource').find('.resource__video-popup').fadeIn();
        // $(this).closest('.resource').find('video, iframe')[0].play();
        $('body').css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });
    $('.resource.videoModule .exit').on('click', function () {
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        });
    });
    $('.resource.videoModule .video-wrapper').on('click', function (e) {
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        });
    });
    $('.resource.videoModule .video-wrapper iframe, .resource.videoModule .video-wrapper video').on('click', function (e) {
        e.stopPropagation();
    });
}

function showResource(openBtn, exitBtn, popupWrapper, popupInner) {
    var button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', function (e) {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        });
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if (afterLoad.length > 0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}

// end file: Storylead/resources.js

// file: Storylead/stl-product-slider.js
$(document).ready(function () {
    $('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Storylead/stl-product-slider.js

// file: Storylead/stl-product-video-slider.js
$(document).ready(function () {
    $('.stl-product-video-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                centerMode: true
            }
        }]
    });

    videoAutoplayFix();
});

$(".stl-product-video-slider a.popup.video-url").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

$(".stl-product-video-slider a.popup.video-embed").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div class="iframe-container"><iframe src="' + videoSrc + '?autoplay=1" frameborder="0" allowfullscreen></iframe></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

$(".stl-product-video-slider a.popup.image").click(function () {
    var imageSrc = $(this).data('image');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div><img width="100%" src="' + imageSrc + '"></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

function videoAutoplayFix() {
    $('.stl-product-video-slider video').each(function () {
        if ($(this).attr('autoplay')) {
            $(this)[0].load();
            $(this)[0].play();
        }
    });

    $('.stl-product-video-slider .vimeo').each(function () {
        var video = $(this)[0];

        //Create a new Vimeo.Player object
        var player = new Vimeo.Player(video);

        //When the player is ready, set the volume to 0
        player.ready().then(function () {
            player.setVolume(0);
        });
    });
}
// end file: Storylead/stl-product-video-slider.js

// file: Website/resources.js

$(document).ready(function () {

    setTimeout(function () {
        $('.resource-categories__element--active').click();
    }, 10);
});

// end file: Website/resources.js
//# sourceMappingURL=template.js.map
