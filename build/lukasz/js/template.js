'use strict';

// file: cutWrapper.js
(function ($) {
    $.fn.cutWrappers = function (targetSelector) {
        $(this).find(targetSelector).appendTo($(this));
        $(this).find('.hs_cos_wrapper_type_custom_widget').remove();
        $(this).find('.hs_cos_wrapper_type_widget_container').remove();
        $(this).find('.widget-span').remove();
    };
})(jQuery);

$(".pricing-box__container").cutWrappers('.pricing-box');
// end file: cutWrapper.js

// file: footer.js
function showPopup(openBtn, exitBtn, popupWrapper, popupInner) {
    var button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', function (e) {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        });
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if (afterLoad.length > 0 && $('.content-wrapper').find(afterLoad).length > 0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}

// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
// 
// Suggested HTML:
// <a href="javascript:void(0);" id="footer-popup" class="cta--white">JETZT ANMELDEN</a>
// <div class="popup-wrapper">
//     <div class="exit"></div>
//     <div class="content-wrapper">
//         {# {% form "footer_form_popup", form_to_use='FormID', overrideable='false', response_message='<span class="h3" style="text-align: center; padding: 0 30px;">Thank you for contacting us. We will get in touch with you as soon as possible.</span>',response_response_type=inline %} #}
//     </div>
// </div>
//
// showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper')
// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// -------------------- SUGGESTED HTML FOR SHOWPOPUP() --------------------
function getScrollBarWidth() {
    var $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body'),
        widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};
$(document).ready(function () {
    showPopup('#footer-popup', '.popup-wrapper .exit', '.popup-wrapper', '.popup-wrapper .content-wrapper');
    showPopup('#subscribe-popup', '.subscribe-popup-wrapper .exit', '.subscribe-popup-wrapper', '.subscribe-popup-wrapper .content-wrapper');
});

// end file: footer.js

// file: forms.js
// Functions for dropdown
function select($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function () {
            // Zmienilem val() na text()
            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text() + '">' + $(this).text() + '</li>');
            }
        });
    });

    $form.find('.dropdown_select.input .dropdown-header').click(function (event, element) {
        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    });
}
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content, .hs_cos_wrapper_type_form", "form", function () {
    $('form select').on('change', function () {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});
// end file: forms.js

// file: header.js
$(document).ready(function () {
    $('.header--main .hs-menu-children-wrapper').each(function () {

        $(this).find('li').wrapAll("<div class='container'></div>");
    });

    headerOnScroll();
    activeMobileMenu();
    slickMenuInt();
    checkMenuLength();
});

function checkMenuLength() {
    $('.hs-menu-item.hs-menu-depth-1.hs-item-has-children').each(function () {
        if ($(this).find('.hs-menu-children-wrapper .container li').length < 4) {
            $(this).find('.hs-menu-children-wrapper .container').addClass('centered');
        }
    });
}
// White background on scroll (landing page)
function headerOnScroll() {
    var header = $('.stl-lp-2017 .header--main');
    var logo = $('.logo').find('img');
    var oldSrc = logo.attr('src');
    var newSrc = oldSrc.replace("-white", "");

    // oldSrc = 'https://www.storylead.com/hs-fs/hubfs/ADMIN/STL_CORPORATE_DESIGN_ELEMENTS/Logo/storylead-new-logo-white.png';
    //  newSrc = 'https://www.storylead.com/hs-fs/hubfs/ADMIN/STL_CORPORATE_DESIGN_ELEMENTS/Logo/storylead-new-logo.png';


    $(window).on("scroll", function () {
        var scrollPosition = $(window).scrollTop();

        if (scrollPosition > 0) {
            header.addClass('header--white');
            logo.attr('src', newSrc);
            logo.attr('srcset', newSrc);
        } else {
            header.removeClass('header--white');
            logo.attr('src', oldSrc);
            logo.attr('srcset', oldSrc);
        }
    });
}

// Resize

$(window).resize(function () {
    activeMobileMenu();
});

// FUNCTIONS

function activeMobileMenu() {

    var deviceWidth = $(window).innerWidth();

    if (deviceWidth < 1140 - 16) {
        $('header').addClass('mobile-menu');
        $('.body-container-wrapper').addClass('mobile-menu');
    } else {
        $('header').removeClass('mobile-menu');
        $('.body-container-wrapper').removeClass('mobile-menu');
    }
}

function slickMenuInt() {

    $('.main-menu .hs-menu-wrapper > ul').slicknav({
        label: '',
        appendTo: '.header--main > .span12 > div:last-child > div',
        allowParentLinks: true,
        init: function init() {
            var old_google_search = $(".slicknav_nav li .hs_cos_wrapper_type_google_search");
            var desktop_google_search = $(".main-menu .hs_cos_wrapper_type_google_search").clone(true);
            old_google_search.after(desktop_google_search);
            old_google_search.remove();
            $(".slicknav_nav li .hs_cos_wrapper_type_google_search input[type='text']").keyup(function () {
                $(".main-menu .hs_cos_wrapper_type_google_search input[type='text']").attr("value", $(this).val());
            });
        }
    });
}

// end file: header.js

// file: template.js
$(document).ready(function () {
    //Fixed Menu
    fixedMenu();
    //Mobile Menu
    mobileMenuInit();
    //Scroll down in header
    headerScrollDown();
    //Scroll up in footer
    footerScrollUp();
    //WOW animation init
    wowInit();

    //ONE, TWO, THREE COLUMN HeroSlider
    smallSliderInit();

    //Homepage HeroSlider
    homeSliderInit();

    //Homepage testimonials
    testimonialsSliderInit();

    //Homepage Services
    var brake_array = {};
    brake_array["1200"] = 4;
    brake_array["992"] = 3;
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    // rowModuleBreaker(".homepage .services",".box-services",brake_array,"first","last");

    //Homepage Features
    var brake_array = {};
    brake_array["768"] = 2;
    brake_array["469"] = 1;
    // rowModuleBreaker(".homepage .features",".feature",brake_array,"first","last");

    //Skills animation
    if ($(".loader1").length > 0 && $(".loader2").length > 0 && $(".loader3").length > 0 && $(".loader4").length > 0) skillsInit();
    questionAccordion();

    //test

    scrollToAnchor();
});
function wowInit() {
    wow = new WOW({
        mobile: false
    });
    wow.init();
}
function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}
function headerScrollDown() {
    $(".mouse_scroll").click(function () {
        $("html, body").animate({
            scrollTop: $("#main_section").position().top - 70
        }, 1000);
        return false;
    });
}
function footerScrollUp() {
    $("#arrow-up").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}
function testimonialsSliderInit() {
    $('.slick-testimonial > div > span').slick({
        dots: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}
function mobileMenuInit() {
    $("header.header .hs-menu-flow-horizontal > ul").slicknav({
        prependTo: "header.header",
        label: "",
        allowParentLinks: true
    });
    $(".slicknav_btn").click(function (i) {
        if ($("header.header.fixed-small").length > 0) {
            $("header.header").removeClass("fixed-small");
        } else {
            $("header.header").addClass("fixed-small");
        }
    });
}
function homeSliderInit() {
    $(".main .homeslider > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function smallSliderInit() {

    $(".sm-slider-main .slide-content > span").slick({
        dots: false,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: "linear",
        responsive: [{
            breakpoint: 767,
            settings: {
                dots: true,
                arrows: false
            }
        }]
    });
}

function fixedMenu() {
    if ($(".header--static").length < 1 && $(".contact-template .header").length == 0) {
        $(window).scroll(function () {
            var i = $(window).scrollTop();
            if (i > 0) {
                $("header.header").addClass("fixed");
                if ($(".menu-black").length > 0) {
                    $(".menu-black .header .black").css("display", "none");
                    $(".menu-black .header .white").css("display", "block");
                }
            } else {
                if (i == 0) {
                    $("header.header").removeClass("fixed");
                    if ($(".menu-black").length > 0) {
                        $(".menu-black .header .black").css("display", "block");
                        $(".menu-black .header .white").css("display", "none");
                    }
                }
            }
        });
    }
    if ($(".contact-template .header").length > 0) {
        $(".contact-template .header").addClass("fixed");
    }
}
function rowModuleBreaker(parent, css_selector, break_array, class_first_row, class_last_row) {
    var oldCols = 0;
    $(window).resize(function () {
        var cols = 0;
        var lastBreak = 0;
        for (var windowBreak in break_array) {
            if (window.innerWidth <= parseInt(windowBreak)) {
                cols = break_array[windowBreak];
                break;
            }
            lastBreak = windowBreak;
        }

        if (cols == 0) {
            cols = break_array[lastBreak];
        }
        if (oldCols == 0 || oldCols != cols) {
            $(parent).each(function (j, selector) {

                var old = oldCols;
                oldCols = cols;
                var inc = -1;
                var row = [];
                var length = $(selector).find(css_selector).length;
                $(selector).find(css_selector).each(function (i, e) {
                    if (i % cols == 0) {
                        inc++;
                        row = [];
                    }
                    row.push($(e).parent().clone(true));
                    $(e).parent().parent().addClass("old_remove");
                    if (i % cols == cols - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        if (i == length - 1) {
                            rowContainer.addClass(class_last_row);
                        }
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }

                    if (i % cols < cols - 1 && i == length - 1) {
                        var rowContainer = $("<div class='row'></div>");
                        if (inc == 0) {
                            rowContainer.addClass(class_first_row);
                        }
                        rowContainer.addClass(class_last_row);
                        rowContainer.append(row);
                        $(e).parent().parent().before(rowContainer);
                    }
                    if ($(e).parent().parent().hasClass("hs_cos_wrapper_type_custom_widget")) {
                        $(e).parent().parent().remove();
                    }
                });
                $(selector).find(css_selector).parent().attr("class", "span" + 12 / cols);
                $(".old_remove").remove();
            });
        }
    });
    $(window).resize();
}
function skillsInit() {
    var r = 0;
    var j = $(".loader1").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var h = $(".loader2").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var g = $(".loader3").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    var f = $(".loader4").ClassyLoader({
        speed: 30,
        diameter: 80,
        roundedLine: true,
        fontSize: "44px",
        fontFamily: "Palanquin",
        fontColor: "#ffffff",
        lineColor: "#ffffff",
        percentage: 0,
        lineWidth: 5,
        start: "top",
        remainingLineColor: "#636362",
        animate: false
    });
    $(window).scroll(function () {
        var w = $(window).scrollTop();
        var t = $(".skills").position().top;
        var z = $(window).height();
        var u = w + z;
        var i = $(".skills .circle1").html();
        var y = $(".skills .circle2").html();
        var x = $(".skills .circle3").html();
        var v = $(".skills .circle4").html();
        if (u - 200 > t && r == 0) {
            r = 1;
            j.setPercent(i).draw();
            h.setPercent(y).draw();
            g.setPercent(x).draw();
            f.setPercent(v).draw();
        }
    });
}

function scrollToAnchor() {
    $('a[href*="#"]').on('click', function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name="' + this.hash.slice(1) + '"]');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 500);
                return false;
            }
        }
    });
}

// end file: template.js

// file: Storylead/browser-check.js
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
if (iOS) {
    $('body').addClass('ios');
}

if (isSafari) {
    $('body').addClass('safari');
}
if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
    $('body').addClass('ms-browser');
}
// end file: Storylead/browser-check.js

// file: Storylead/count-to.js
$(window).scroll(function () {

    checkCounterPosition();
});

$(document).ready(function () {

    checkCounterPosition();
});

// VARIABELS 


var checkcounter = 0;

function countTo() {

    $('.counter-box-stl .count-to').each(function () {

        var $this = $(this);
        var countTo = $this.attr('data-count');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        }, {

            duration: 4000,
            easing: 'swing',
            step: function step() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function complete() {
                $this.text(this.countNum);
            }

        });
    });
    checkcounter = 1;
}

function checkCounterPosition() {
    var counterposition;
    if ($(".counter-box-stl .count-to").length != 0) {
        counterposition = $(".counter-box-stl .count-to").position().top + 100;
    }

    if ($(".counter-background .counter-box-stl .count-to").length != 0) {
        counterposition = $(".counter-background ").position().top + 200;
    }
    if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
        countTo();
    }
}
// end file: Storylead/count-to.js

// file: Storylead/global.js

$('#arrow-up').on('click', function () {
    $('body, html').animate({
        scrollTop: 0
    }, 500);
});
// end file: Storylead/global.js

// file: Storylead/home.js
$(document).ready(function () {
    referenzeSlider();
});

function referenzeSlider() {
    $('.referenzen-slider-wrapper > span').slick({
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 8000,
        speed: 1000,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 960,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 720,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 490,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]

    });
}
// end file: Storylead/home.js

// file: Storylead/isotope_uber_uns.js
// file: Modules/single-themen.item.js
$(document).ready(function () {
    $('.container.filtration-wrapper').cutWrappers('.single-themen-item');
    // first run isotope
    setTimeout(function () {
        $('.filtration-wrapper').isotope();
    }, 10);

    skillsInit2();
});

// end file: Modules/single-themen.item.js
// $(document).ready(function () {
//     //Skills animation
//     if ($(".loading-skills-wrapper").length > 0) {
//         skillsInit();
//     }
// });

function skillsInit() {
    var checkBars = 0;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if ($(".loading-skills-wrapper").length > 0) {
            loadingBars();
        }
    });
    $(window).scroll();

    function loadingBars() {
        var scrollTop = $(window).scrollTop();
        var positionBars = $(".loading-circle").offset().top;
        var windowHeight = $(window).height();
        var scrollBottom = scrollTop + windowHeight;
        if (scrollBottom - 150 > positionBars && checkBars == 0) {
            checkBars = 1;

            $("section.loading-circle .circle").circliful({
                animation: 1,
                animationStep: 5,
                foregroundBorderWidth: 3,
                backgroundBorderWidth: 3,
                foregroundColor: '#FFF',
                backgroundColor: '#636362',
                fontColor: '#FFF',
                percentageTextSize: 30,
                multiPercentage: 1
            });
            $("section.loading-circle .circle .timer").attr('y', '110');
        }
    };
}

function skillsInit2() {

    $('.single-themen-item').hover(function () {

        if (!$(this).hasClass('initiated')) {
            $(this).addClass('initiated');
            loadingBars2($(this));
        }
    });

    function loadingBars2(elem) {
        var percentBar1 = $(elem).find(".bar1 span").html();
        var percentBar2 = $(elem).find(".bar2 span").html();
        var percentBar3 = $(elem).find(".bar3 span").html();
        $(elem).find(".bar span").html(0 + "%");

        $(elem).find(".wrapper-bars .loading-bar").css('width', 0 + "%");

        setTimeout(function () {
            $(elem).find(".wrapper-bars .bar1 .loading-bar").css('width', percentBar1 + "%");
            $(elem).find(".bar1 span").html(percentBar1 + "%");
        }, 100);
        setTimeout(function () {
            $(elem).find(".wrapper-bars .bar2 .loading-bar").css('width', percentBar2 + "%");
            $(elem).find(".bar2 span").html(percentBar2 + "%");
        }, 500);
        setTimeout(function () {
            $(elem).find(".wrapper-bars .bar3 .loading-bar").css('width', percentBar3 + "%");
            $(elem).find(".bar3 span").html(percentBar3 + "%");
        }, 900);
    }
}

// end file: Storylead/isotope_uber_uns.js

// file: Storylead/resources.js
$(document).ready(function () {
    $('.resource-categories__list.second-list').closest('li').width($('.resource-categories__list.second-list').outerWidth() - 40);
    $('.resource-categories__list.first-list').closest('li').width($('.resource-categories__list.first-list').outerWidth() - 40);
    $('.resource-categories__list.first-list li').on('click', function () {
        var clickText = $(this).text();
        $('#first-list-expand-btn').text(clickText);
    });
    $('.resource-categories__list.second-list li').on('click', function () {
        var clickText = $(this).text();
        $('#second-list-expand-btn').text(clickText);
    });

    toggleMenus('#first-list-expand-btn', '.resource-categories__list.first-list');
    toggleMenus('#second-list-expand-btn', '.resource-categories__list.second-list');
    showResourcePopup();
});
function toggleMenus(btn, listToToggle) {
    $(btn).on('click', function () {
        $(listToToggle).fadeToggle();
    });
    $(listToToggle + ' li').on('click', function () {
        $(listToToggle + ' li').parent().fadeOut();
    });
}
function showResourcePopup() {
    $('.resource.videoModule .resource-play-video-top, .resource.videoModule .resource-play-video').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.resource').find('.resource__video-popup').fadeIn();
        // $(this).closest('.resource').find('video, iframe')[0].play();
        $('body').css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });
    $('.resource.videoModule .exit').on('click', function () {
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        });
    });
    $('.resource.videoModule .video-wrapper').on('click', function (e) {
        $(this).closest('.resource').find('.resource__video-popup').fadeOut();
        // $(this).closest('.resource').find('video, iframe')[0].pause();
        $('body').css({
            'overflow': '',
            'margin-right': 0
        });
    });
    $('.resource.videoModule .video-wrapper iframe, .resource.videoModule .video-wrapper video').on('click', function (e) {
        e.stopPropagation();
    });
}

function showResource(openBtn, exitBtn, popupWrapper, popupInner) {
    var button = $(openBtn),
        modal = $(popupWrapper),
        form = $(popupInner),
        exit = $(exitBtn),
        body = $('body'),
        afterLoad = $('a[name="' + location.hash.replace('#', '') + '"]');

    button.on('click', function (e) {
        e.preventDefault();
        modal.fadeIn();
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    });

    exit.add(modal).on('click', function () {
        modal.fadeOut();
        body.css({
            'overflow': '',
            'margin-right': 0
        });
    });

    form.on('click', function (e) {
        e.stopPropagation();
    });

    if (afterLoad.length > 0) {
        afterLoad.closest(modal).fadeIn(700);
        body.css({
            'overflow': 'hidden',
            'margin-right': getScrollBarWidth()
        });
    }
}

// end file: Storylead/resources.js

// file: Storylead/stl-product-slider.js
$(document).ready(function () {
    $('.stl-product-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 991,
            settings: {
                arrows: false,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 767,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});
// end file: Storylead/stl-product-slider.js

// file: Storylead/stl-product-video-slider.js
$(document).ready(function () {
    $('.stl-product-video-slider .hs_cos_wrapper_type_widget_container').slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: false,
        arrows: false,
        slidesToShow: 4,
        variableWidth: true,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
                centerMode: true
            }
        }]
    });

    videoAutoplayFix();
});

$(".stl-product-video-slider a.popup.video-url").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

$(".stl-product-video-slider a.popup.video-embed").click(function () {
    var videoSrc = $(this).data('video');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div class="iframe-container"><iframe src="' + videoSrc + '?autoplay=1" frameborder="0" allowfullscreen></iframe></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

$(".stl-product-video-slider a.popup.image").click(function () {
    var imageSrc = $(this).data('image');
    var $this = $(this);

    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<div><img width="100%" src="' + imageSrc + '"></div>'
        },
        callbacks: {
            open: function open() {
                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
            }
        }
    });
});

function videoAutoplayFix() {
    $('.stl-product-video-slider video').each(function () {
        if ($(this).attr('autoplay')) {
            $(this)[0].load();
            $(this)[0].play();
        }
    });

    $('.stl-product-video-slider .vimeo').each(function () {
        var video = $(this)[0];

        //Create a new Vimeo.Player object
        var player = new Vimeo.Player(video);

        //When the player is ready, set the volume to 0
        player.ready().then(function () {
            player.setVolume(0);
        });
    });
}
// end file: Storylead/stl-product-video-slider.js

// file: Storylead/uber-team.js
$("a.play").click(function () {

    var videoSrc = $(this).data('video');
    var $this = $(this);
    $.magnificPopup.open({
        items: {
            type: 'inline',
            src: '<video controls preload="auto" width="100%" height="100%" src="' + videoSrc + '"></video>'
        },
        callbacks: {
            open: function open() {

                $('html').css('margin-right', 0);
                $this.parent().parent().addClass("hover");
                $(this.content)[0].play();
            },
            close: function close() {
                $this.parent().parent().removeClass("hover");
                $(this.content)[0].load();
            }
        }
    });
});

setTimeout(function () {
    $('.filters li a.active').click();
}, 10);
$(".questions").append($(".questions .question"));
$(".questions >.span12").remove();
$(".filters").append($(".filters .filter"));
$(".filters >.span12").remove();
var $container = $('.questions');

// filter items when filter link is clicked
$('.filters li a').on('click', function () {
    $(".faq .questions .question").css('position', 'absolute');
    $(".faq .questions .question h3").removeClass('rotate');
    $(".faq .questions .question div.text").hide(true);
    $('.filters li a').removeClass('active');
    $(this).addClass('active');
    var selector = $(this).data('filter');
    $container.isotope({
        filter: selector
    });
    return false;
});

if ($(".faq").length > 0) {
    $(".faq .questions .question h3").click(function (i) {
        if ($(this).hasClass("rotate")) {
            $(this).parent().children("div.text").slideUp("fast");
            $(this).removeClass("rotate");
        } else {
            $(".faq .questions .question div.text").slideUp("fast");
            $(".faq .questions .question h3").removeClass("rotate");
            $(this).addClass("rotate");
            $(this).parent().children("div.text").slideDown("fast");
            $(".faq .questions .question").css("position", "static");
        }
    });
}

// end file: Storylead/uber-team.js

// file: Website/resources.js

$(document).ready(function () {

    setTimeout(function () {
        $('.resource-categories__element--active').click();
    }, 10);
});

// end file: Website/resources.js

// file: Website/stl-product-tables.js
$(document).ready(function () {
    productTableInit();
    changeProductTable();
    productAccordions();
});

function productTableInit() {
    var navItem = 0;
    var tableItem = 0;
    $('.product-table-left-nav__item').each(function () {
        $(this).attr('data-item', navItem);
        navItem++;
    });

    $('.product-table').each(function () {
        $(this).attr('data-item', tableItem);

        if ($(this).siblings('.product-related').length > 0) {
            $(this).siblings('.product-related').attr('data-item', tableItem);
            $('.product-add').append($(this).siblings('.product-related'));
        }

        tableItem++;
    });
}

function changeProductTable() {
    var activeTableId = $('.product-table-left-nav__item--active').data('item');
    var activeTableName = $('.product-table-left-nav__item--active').attr('id');

    $('.product-table[data-item=' + activeTableId + ']').addClass('product-table--active').fadeIn().show();
    $('.product-related[data-item=' + activeTableId + ']').addClass('product-related--active').fadeIn().show();

    $('.product-table-left-nav__item').on('click', function () {
        if ($(this).hasClass('product-table-left-nav__item--active')) {
            return false;
        } else {
            $('.product-table-left-nav__item').removeClass('product-table-left-nav__item--active');
            $(this).addClass('product-table-left-nav__item--active');
            activeTableId = $(this).data('item');
            $('.product-table').removeClass('product-table--active').fadeOut().hide();
            $('.product-related').removeClass('product-related--active').fadeOut().hide();;
            $('.product-table[data-item=' + activeTableId + ']').addClass('product-table--active').fadeIn().show();
            $('.product-related[data-item=' + activeTableId + ']').addClass('product-related--active').fadeIn().show();

            activeTableName = $('.product-table-left-nav__item--active').attr('id');
            var changeUrl = location.protocol + "//" + location.host + location.pathname + "?selected=" + activeTableName;
            window.history.replaceState({}, document.title, changeUrl);
        }

        if ($(window).width() < 768) {
            $('html, body').animate({
                scrollTop: $('.product-table[data-item=' + activeTableId + ']').first().offset().top - 100
            }, 500);
        }
    });

    if (GetURLParameter("selected") != undefined) {
        var selectedTable = GetURLParameter("selected");
        $('.product-table-left-nav__item#' + selectedTable).click();
        var changeUrl = location.protocol + "//" + location.host + location.pathname + "?selected=" + activeTableName;
        window.history.replaceState({}, document.title, changeUrl);
    }
}

function productAccordions() {
    $('.product-table__accordions a').on('click', function (e) {
        e.preventDefault();
    });

    $('.product-table__accordions .accordion-depth-1 > a').on('click', function (e) {
        if ($(this).parents('.accordion-depth-1').hasClass('expand')) {
            $('.product-table__accordions .accordion-depth-1').removeClass('expand');
            $('.product-table__accordions .accordion-depth-1 ul').slideUp();
        } else {
            $('.product-table__accordions .accordion-depth-1').removeClass('expand');
            $('.product-table__accordions .accordion-depth-1 ul').slideUp();
            $(this).parents('.accordion-depth-1').addClass('expand');
            $(this).parents('.accordion-depth-1').find('ul').slideDown();
        }
    });
}

function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}
// end file: Website/stl-product-tables.js

// file: Landing/event-lp5.js
$(document).ready(function () {

    initBrandsSlider();
});

function initBrandsSlider() {
    if ($(".brands-slider").length > 0) {
        $('.brands-slider').cutWrappers('.brands-slider__slide');
        $('.brands-slider').slick({
            autoplay: true,
            autoplaySpeed: 2000,
            slidesToShow: 5,
            slidesToScroll: 5,
            arrows: true,
            dots: true,
            responsive: [{
                breakpoint: 1150,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 900,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }
}
// end file: Landing/event-lp5.js

// file: Landing/stl-survey.js
$(window).load(function () {
    if ($('.stl-survey').hasClass('stl-survey-questions')) {
        $('.stl-survey-hero-banner__icon-box').first().addClass('stl-survey-hero-banner__icon-box--active');
    } else if ($('.stl-survey').hasClass('stl-survey-answers') == true) {
        highlightAnswerOnion();
    } else if ($('.stl-survey').hasClass('stl-survey-typ') == true) {
        $('.stl-survey-hero-banner__icon-box').addClass('stl-survey-hero-banner__icon-box--active');
    }
    addRangeInputs();
    makeSections();
    changeSection();
    changeHiddenInputVal();
});

function highlightAnswerOnion() {
    $('.stl-survey-answer').each(function () {
        var sectionNum = parseInt($(this).data('section')) + 1;
        $('.stl-survey-hero-banner__icon-box:nth-of-type(' + sectionNum + ')').addClass('stl-survey-hero-banner__icon-box--active');
    });
}

function addRangeInputs() {
    $('[class^="hs_business_check_negative"], [class^="hs_business_check_positive"]').each(function () {
        $(this).append('<div class="stl-survey-range-slider-wrapper">' + '<div class="stl-survey-range-slider-labels"><p>UNZUFRIEDEN</p><p>ZUFRIEDEN</p></div>' + '<input class="stl-survey-range-slider stl-survey-range-slider--unchanged" type="range" min="0" max="10" value="0">' + '<div class="stl-survey-range-slider-legend"><span>0</span><span>1</span><span>2</span><span>3</span><span>4</span><span>5</span><span>6</span><span>7</span><span>8</span><span>9</span><span>10</span></div>' + '</div>');
    });
}

function makeSections() {
    for (var i = 1; i <= 5; i++) {
        $('.stl-survey-body form').append('<div class="stl-survey-section stl-survey-section-' + i + '"></div>');
    }

    $('.stl-survey-section').first().addClass('stl-survey-section--active');

    var sectionStory = $('[name^="business_check_"][name$="_story"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
    var sectionLeads = $('[name^="business_check_"][name$="_leads"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
    var sectionDeals = $('[name^="business_check_"][name$="_deals"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();
    var sectionGrowth = $('[name^="business_check_"][name$="_growth"]:not([name^="business_check_vop_"])').parents('.hs-form-field').parent();

    $('.stl-survey-section-1').append(sectionStory);
    $('.stl-survey-section-2').append(sectionLeads);
    $('.stl-survey-section-3').append(sectionDeals);
    $('.stl-survey-section-4').append(sectionGrowth);

    //append all other fields to last section
    $('.stl-survey-body form > fieldset').each(function () {
        $('.stl-survey-section-5').append($(this));
    });

    $('.stl-survey-questions .widget-type-form form').css('display', 'block');
}

function validateForm() {
    if ($('.stl-survey-section--active [name^="business_check_positive"]').val() != '' && $('.stl-survey-section--active [name^="business_check_negative"]').val() != '') {
        return true;
    } else {
        return false;
    }
}

function changeHiddenInputVal() {
    $('input.stl-survey-range-slider').on('input change, click', function () {
        $(this).removeClass('stl-survey-range-slider--unchanged');
        var rangeVal = $(this).val();
        var hiddenInput = $(this).parent().siblings('.input').find('input.hs-input');
        hiddenInput.attr('value', rangeVal);

        if (validateForm() == true) {
            $('.stl-survey-nav .btn-next').removeClass('btn-disabled');
        }

        if ($(window).width() < 767) {
            var legend = $(this).siblings('.stl-survey-range-slider-legend');
            legend.find('span').css('opacity', '0');
            legend.find('span:nth-child(' + (parseInt(rangeVal) + 1) + ')').css('opacity', '1');
        } else {
            $('.stl-survey-range-slider-legend span').removeAttr('style');
        }
    });
}

function calculateVop() {
    var negativeStory = parseInt($('input[name$=_negative_story]').val());
    var positiveStory = parseInt($('input[name$=_positive_story]').val());
    var vopStory = $('input[name$=_vop_story]');
    var vopStoryResult = positiveStory - negativeStory;

    var negativeLeads = parseInt($('input[name$=_negative_leads]').val());
    var positiveLeads = parseInt($('input[name$=_positive_leads]').val());
    var vopLeads = $('input[name$=_vop_leads]');
    var vopLeadsResult = positiveLeads - negativeLeads;

    var negativeDeals = parseInt($('input[name$=_negative_deals]').val());
    var positiveDeals = parseInt($('input[name$=_positive_deals]').val());
    var vopDeals = $('input[name$=_vop_deals]');
    var vopDealsResult = positiveDeals - negativeDeals;

    var negativeGrowth = parseInt($('input[name$=_negative_growth]').val());
    var positiveGrowth = parseInt($('input[name$=_positive_growth]').val());
    var vopGrowth = $('input[name$=_vop_growth]');
    var vopGrowthResult = positiveGrowth - negativeGrowth;

    var highestVopInput = $('input[name=business_check_highest_vop]');
    var highestVop = 0;
    var highestVopSection = "";

    var vopArray = {
        Story: vopStoryResult,
        Leads: vopLeadsResult,
        Deals: vopDealsResult,
        Growth: vopGrowthResult
    };

    $.each(vopArray, function (key, value) {
        if (value > highestVop) {
            highestVop = value;
            highestVopSection = key;
        }
    });

    highestVopInput.val(highestVopSection);

    vopStory.val(vopStoryResult);
    vopLeads.val(vopLeadsResult);
    vopDeals.val(vopDealsResult);
    vopGrowth.val(vopGrowthResult);
}

function changeSection() {
    $('.stl-survey-nav .btn-next').on('click', function () {

        // trigger click on form hidden submit button when in last section
        if ($('.stl-survey-section-5').hasClass('stl-survey-section--active')) {
            calculateVop();
            $('.stl-survey-body .hs_submit input[type="submit"]').click();
        }

        if (validateForm() == true && $('.stl-survey-section--active').next('.stl-survey-section').length > 0) {

            if ($('.stl-survey-section--active').hasClass('stl-survey-section--valid') == false) setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $('.stl-survey-section--active').offset().top - 100
                }, 500);
            }, 750);

            var nextSection = $('.stl-survey-section--active').next('.stl-survey-section');
            $('.stl-survey-section--active').addClass('stl-survey-section--valid');
            $('.stl-survey-section').removeClass('fadeOutRight');
            $('.stl-survey-section').removeClass('fadeInLeft');
            $(".stl-survey-section--active").addClass("wow fadeOutLeft animated").attr('data-wow-duration', '0.5s');
            $(".stl-survey-section--active").attr("style", "visibility: visible; animation-name: fadeOutLeft;");
            $('.stl-survey-section').removeClass('stl-survey-section--active');
            nextSection.addClass('stl-survey-section--active');
            $(".stl-survey-section--active").addClass("wow fadeInRight animated").attr('data-wow-duration', '0.5s');;
            $(".stl-survey-section--active").attr("style", "visibility: visible; animation-name: fadeInRight;");

            $('.stl-survey-nav .btn-prev').removeClass('btn-disabled');
            $('.stl-survey-nav .btn-next').addClass('btn-disabled');

            if ($('.stl-survey-section-5').hasClass('stl-survey-section--active') == false) {
                $('.stl-survey-hero-banner__icon-box--active').find('.inner-rect-mask').addClass('inner-rect-mask--active');
                var nextHeroIcon = $('.stl-survey-hero-banner__icon-box--active').next('.stl-survey-hero-banner__icon-box');

                if ($(window).width() > 767) {
                    nextHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
                } else {
                    $('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').removeClass('stl-survey-hero-banner__icon-box--active');
                    nextHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
                }
            }

            // validate again for next section. If true, btn will be clickable
            if (validateForm() == true) {
                $('.stl-survey-nav .btn-next').removeClass('btn-disabled');
            }
        }
    });
    $('.stl-survey-nav .btn-prev').on('click', function () {
        if ($('.stl-survey-section-1.stl-survey-section--active').length == 0 && $('.stl-survey-section--active').prev('.stl-survey-section').length > 0) {
            var prevSection = $('.stl-survey-section--active').prev('.stl-survey-section');

            if ($('.stl-survey-section-5').hasClass('stl-survey-section--active') == false) {
                $('.inner-rect-mask.inner-rect-mask--active').last().removeClass('inner-rect-mask--active');

                if ($(window).width() > 767) {
                    $('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').last().removeClass('stl-survey-hero-banner__icon-box--active');
                } else {
                    var prevHeroIcon = $('.stl-survey-hero-banner__icon-box--active').last().prev('.stl-survey-hero-banner__icon-box');
                    $('.stl-survey-hero-banner__icon-box.stl-survey-hero-banner__icon-box--active').removeClass('stl-survey-hero-banner__icon-box--active');
                    prevHeroIcon.addClass('stl-survey-hero-banner__icon-box--active');
                }
            }

            $('.stl-survey-section').removeClass('fadeOutLeft');
            $('.stl-survey-section').removeClass('fadeInRight');
            $(".stl-survey-section--active").addClass("wow fadeOutRight animated");
            $(".stl-survey-section--active").attr("style", "visibility: visible; animation-name: fadeOutRight;");
            $('.stl-survey-section').removeClass('stl-survey-section--active');
            prevSection.addClass('stl-survey-section--active');
            $(".stl-survey-section--active").addClass("wow fadeInLeft animated");
            $(".stl-survey-section--active").attr("style", "visibility: visible; animation-name: fadeInLeft;");

            $('.stl-survey-nav .btn-next').removeClass('btn-disabled');
            $('.stl-survey-nav .btn-prev').removeClass('btn-disabled');

            // always lock btn for first section
            if ($('.stl-survey-section-1.stl-survey-section--active').length > 0) {
                $('.stl-survey-nav .btn-prev').addClass('btn-disabled');
            }
        }
    });
}
// end file: Landing/stl-survey.js

// file: Modules/timeline-item.js


// end file: Modules/timeline-item.js
//# sourceMappingURL=template.js.map
